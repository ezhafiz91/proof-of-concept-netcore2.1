﻿
namespace Poc.Switch.Feature.GoogleCalendar.Controller
{
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Google.Apis.Auth.OAuth2;
    using Services.Interfaces;
    using Models.Requests;
    using Extensions;

    [Route("api/[controller]")]
    [ApiController]
    public class GoogleCalendarController
    {
        private readonly IGoogleCalendarApiService _googleCalendarApiService;
       
        public GoogleCalendarController(IGoogleCalendarApiService googleCalendarApiService,
            IHostingEnvironment hostingEnvironment)
        {
            _googleCalendarApiService = googleCalendarApiService;
        }

        [HttpGet("GetCalendar")]
        public JsonResult GetCalendar()
        {
            var credentials = $"{Directory.GetCurrentDirectory()}\\Configuration\\credentials.json";

            var userCredentials = _googleCalendarApiService.GetCredentialsUsingFileStream(credentials);

            if (userCredentials == null) return null;

            var service = _googleCalendarApiService.GetGoogleCalendarService(userCredentials);

            var eventList = _googleCalendarApiService.GetEvents(service);

            return new JsonResult(eventList);
        }

        [HttpGet("GetCalendarUsingCredentials")]
        public JsonResult GetCalendarUsingCredentials([FromBody] string credentials)
        {
            UserCredential userCredentials = _googleCalendarApiService.GetCredentialsUsingMemoryStream(credentials);

            if (userCredentials == null) return null;

            var service = _googleCalendarApiService.GetGoogleCalendarService(userCredentials);

            var eventList = _googleCalendarApiService.GetEvents(service);

            return new JsonResult(eventList);
        }

        [HttpPost("CreateEvent")]
        public async Task<JsonResult> CreateEvent([FromBody] CreateEvent requestEventItem)
        {
            var credentials = $"{Directory.GetCurrentDirectory()}\\Configuration\\credentials.json";

            var userCredentials = _googleCalendarApiService.GetCredentialsUsingFileStream(credentials);

            if (userCredentials == null) return null;

            var service = _googleCalendarApiService.GetGoogleCalendarService(userCredentials);

            var eventItem = EventItemExtension.BuildEventItem(requestEventItem);

            var createtedEvent = await _googleCalendarApiService.CreateEvent(service, eventItem);

            if(createtedEvent == null) return new JsonResult("Event failed on creation");

            return new JsonResult(eventItem);
        }

    }
}
