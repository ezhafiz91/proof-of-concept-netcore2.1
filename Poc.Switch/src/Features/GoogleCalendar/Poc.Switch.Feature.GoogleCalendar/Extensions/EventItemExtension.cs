﻿namespace Poc.Switch.Feature.GoogleCalendar.Extensions
{
    using System;
    using System.Collections.Generic;
    using Google.Apis.Calendar.v3.Data;
    using Enumerators;
    using Models.Requests;

    public static class EventItemExtension
    {
        public static Event BuildEventItem(CreateEvent requestEventItem)
        {

            var attendees = new List<EventAttendee>();

            foreach (var attendee in requestEventItem.Attendees)
            {
                var eventAttendee = new EventAttendee()
                {
                    Email = attendee.Email
                };
                attendees.Add(eventAttendee);
            }

            var recurrence = new List<string> { requestEventItem.Recurrence };

            var eventItem = new Event()
            {
                Summary = requestEventItem.Summary,
                Location = requestEventItem.Location,
                Description = requestEventItem.Description,
                Start = new EventDateTime()
                {
                    DateTime = DateTime.Parse(requestEventItem.Start),
                    TimeZone = Constants.Timezone
                },
                End = new EventDateTime()
                {
                    DateTime = DateTime.Parse(requestEventItem.End),
                    TimeZone = "Australia/Sydney"
                },
                Recurrence = recurrence,
                Attendees = attendees
            };

            return eventItem;
        }

    }
}
