﻿namespace Poc.Switch.Feature.GoogleCalendar.Models.Requests
{
    public class Attendee
    {
        public string Email { get; set; }
    }
}
