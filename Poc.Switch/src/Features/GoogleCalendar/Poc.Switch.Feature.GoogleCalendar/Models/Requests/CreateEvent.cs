﻿namespace Poc.Switch.Feature.GoogleCalendar.Models.Requests
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class CreateEvent
    {
        public string Summary { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Recurrence { get; set; }
        public List<Attendee> Attendees { get; set; }
    }
}
