﻿namespace Poc.Switch.Feature.GoogleCalendar.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Google.Apis.Calendar.v3.Data;

    public class EventItem
    {
        public string Summary { get; set; }
        public string When { get; set; }

    }
}
