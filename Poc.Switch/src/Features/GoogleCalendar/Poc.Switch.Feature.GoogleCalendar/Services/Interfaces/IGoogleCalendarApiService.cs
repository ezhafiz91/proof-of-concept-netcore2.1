﻿using Microsoft.AspNetCore.Mvc;

namespace Poc.Switch.Feature.GoogleCalendar.Services.Interfaces
{
    using System.IO;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Google.Apis.Calendar.v3.Data;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Calendar.v3;
    using Models;

    public interface IGoogleCalendarApiService
    {
        UserCredential GetCredentialsUsingMemoryStream(string credentialsToken);
        UserCredential GetCredentialsUsingFileStream(string credentialsToken);
        CalendarService GetGoogleCalendarService(UserCredential credential);
        List<EventItem> GetEvents(CalendarService service);
        Task<Event> CreateEvent(CalendarService service, Event eventItem);
    }
}
