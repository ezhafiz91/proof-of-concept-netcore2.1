﻿using System.Text;
using System.Threading.Tasks;
using Google.Apis.Calendar.v3.Data;
using Microsoft.AspNetCore.Mvc;

namespace Poc.Switch.Feature.GoogleCalendar.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using Google.Apis.Util.Store;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Calendar.v3;
    using Google.Apis.Services;
    using Interfaces;
    using Enumerators;
    using Models;
    
    public class GoogleCalendarApiService : IGoogleCalendarApiService
    {
        public UserCredential GetCredentialsUsingMemoryStream(string credentials)
        {
            var byteArray = Encoding.UTF8.GetBytes(credentials);

            var credentialsStream = new MemoryStream(byteArray);

            UserCredential userCredentials = null;
            var credentialPath = Constants.CredentialsToken;
            userCredentials = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(credentialsStream).Secrets,
                Constants.Scopes,
                Constants.CredentialsType,
                CancellationToken.None,
                new FileDataStore(credentialPath, true)).Result;

            return userCredentials;
        }

        public UserCredential GetCredentialsUsingFileStream(string credentialsToken)
        {
            UserCredential userCredentials = null;
            var credentialPath = Constants.CredentialsToken;
            using (var stream = new FileStream(credentialsToken, FileMode.Open, FileAccess.Read))
            {
                userCredentials = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Constants.Scopes,
                    Constants.CredentialsType,
                    CancellationToken.None,
                    new FileDataStore(credentialPath, true)).Result;
            }

            return userCredentials;
        }
        
        public CalendarService GetGoogleCalendarService(UserCredential credential)
        {
            var googleService = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Constants.ApplicationName
            });

            return googleService;

        }

        public List<EventItem> GetEvents(CalendarService service)
        {
            var request = service.Events.List(Constants.EventType);
            request.TimeMin = DateTime.Now;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.MaxResults = 10;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

            var events = request.Execute();

            if (events.Items == null || events.Items.Count <= 0) return null;

            var eventList = new List<EventItem>();

            foreach (var eventItem in events.Items)
            {
                var when = eventItem.Start.DateTime.ToString();
                if (string.IsNullOrEmpty(when))
                {
                    when = eventItem.Start.Date;
                }
                
                var eventItemString = new EventItem()
                {
                    Summary = eventItem.Summary,
                    When = when
                };

                eventList.Add(eventItemString);
            }

            return eventList;
        }

        public async Task<Event> CreateEvent(CalendarService service, Event eventItem)
        {
            var serviceRequest = service.Events.Insert(eventItem, Constants.EventType);
            var createdEvent = serviceRequest.Execute();

            return createdEvent;
        }
    }
}
