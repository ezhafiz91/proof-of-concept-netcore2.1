﻿namespace Poc.Switch.Feature.GoogleCalendar.Enumerators
{
    using Google.Apis.Calendar.v3;

    public class Constants
    {
        public const string EventType = "primary";
        public const string CredentialsToken = "token.json";
        public const string CredentialsType = "user";
        public const string Timezone = "Australia/Sydney";

        // Statics
        public static string ApplicationName = "Switch Google Calendar POC";
        public static string[] Scopes = { CalendarService.Scope.CalendarEvents };

    }
}
