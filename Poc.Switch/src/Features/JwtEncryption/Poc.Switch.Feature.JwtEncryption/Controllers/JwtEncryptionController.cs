﻿namespace Poc.Switch.Feature.JwtEncryption.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Poc.Switch.Feature.JwtEncryption.Services.Interfaces;
    using Poc.Switch.Foundation.Configuration.Enumerators;
    using Poc.Switch.Foundation.Configuration.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class JwtEncryptionController
    {
        private readonly IJWTService _jwtService;
        public JwtEncryptionController(IJWTService jwtService)
        {
            _jwtService = jwtService;
        }

        // POST api/values
        [HttpPost("encrypt")]
        public ActionResult<string> Post([FromBody] RequestBody requestBody)
        {
            if (requestBody == null) return "Invalid requestBody";

            var encoder = _jwtService.GetJwtEncoder();

            var token = _jwtService.GetEncodedToken(encoder, requestBody, Constants.SecretKey);

            return token;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="secretKey"></param>
        /// <param name="requestToken"></param>
        /// <returns></returns>
        [HttpPost("decrypt")]
        public JsonResult Post(string secretKey, [FromBody] string requestToken)
        {
            if (string.IsNullOrWhiteSpace(requestToken)) return new JsonResult("Invalid Request body");

            if (string.IsNullOrWhiteSpace(secretKey)) return new JsonResult($"Invalid secret key - cannot decrypt {requestToken}");

            var decoder = _jwtService.GetJwtDecoder();

            var requestBody = _jwtService.GetDecodedToken<RequestBody>(decoder, requestToken, secretKey);

            if(requestBody == null) return new JsonResult("Json object cannot be identified");

            return new JsonResult(requestBody);

        }
    }
}
