﻿namespace Poc.Switch.Feature.JwtEncryption.Services.Interfaces
{
    using JWT;
    using Poc.Switch.Foundation.Configuration.Models;

    public interface IJWTService
    {
        IJwtEncoder GetJwtEncoder();

        IJwtDecoder GetJwtDecoder();

        string GetEncodedToken(IJwtEncoder encoder, RequestBody requestBody, string secretKey);

        T GetDecodedToken<T>(IJwtDecoder decoder, string token, string secretKey);
    }
}
