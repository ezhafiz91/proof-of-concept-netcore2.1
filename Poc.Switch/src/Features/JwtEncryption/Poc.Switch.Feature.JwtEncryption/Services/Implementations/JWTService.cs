﻿namespace Poc.Switch.Feature.JwtEncryption.Services.Implementations
{
    using JWT;
    using JWT.Algorithms;
    using JWT.Serializers;
    using Newtonsoft.Json;
    using Poc.Switch.Foundation.Configuration.Models;
    using Poc.Switch.Feature.JwtEncryption.Services.Interfaces;

    public class JWTService : IJWTService
    {
        public IJwtEncoder GetJwtEncoder()
        {
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();

            return new JwtEncoder(algorithm, serializer, urlEncoder);
        }

        public IJwtDecoder GetJwtDecoder()
        {
            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            return new JwtDecoder(serializer, validator, urlEncoder);
        }

        public string GetEncodedToken(IJwtEncoder encoder, RequestBody requestBody, string secretKey)
        {
            return encoder.Encode(requestBody, secretKey);
        }

        public T GetDecodedToken<T>(IJwtDecoder decoder, string token, string secretKey)
        {
            var json = decoder.Decode(token, secretKey, true);
            
            return JsonConvert.DeserializeObject<T>(json);
        }

        public RequestBody Test()
        {
            return null;
        }
    }
}
