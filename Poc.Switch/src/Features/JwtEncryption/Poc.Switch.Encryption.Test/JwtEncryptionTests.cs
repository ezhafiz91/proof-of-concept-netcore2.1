namespace Poc.Switch.Encryption.Test
{
    using System;
    using Xunit;
    using JWT;

    using Poc.Switch.Feature.JwtEncryption.Services.Implementations;
    using Poc.Switch.Foundation.Configuration.Models;

    public class JwtEncryptionTests
    {
        [Fact]
        public void GetEncoder()
        {
            var encryptionService = new JWTService();
            var encoder = encryptionService.GetJwtEncoder();

            Assert.IsType<JwtEncoder>(encoder);
        }

        [Fact]
        public void GetDecoder()
        {
            var encryptionService = new JWTService();
            var decoder = encryptionService.GetJwtDecoder();

            Assert.IsType<JwtDecoder>(decoder);
        }

        [Fact]
        public void GetEncryptedToken()
        {
            // Build mock data
            var encryptionService = new JWTService();
            var encoder = encryptionService.GetJwtEncoder();

            var mockedRequestBody = new RequestBody()
            {
                bankDetails = new BankDetails()
                {
                    AccountNo = "09882099",
                    SortCode = "989898"
                },
                details = new Details()
                {
                    DateOfBirth = "2001-09-01",
                    FirstName = "Ez",
                    LastName = "Hafiz"
                }
            };

            // Act
            var result = encryptionService.GetEncodedToken(encoder, mockedRequestBody, "ThisIsASecretKey");

            // Assert
            Assert.IsType<string>(result);
        }

        [Fact]
        public void GetDecodedToken()
        {
            var encryptionService = new JWTService();
            var decoder = encryptionService.GetJwtDecoder();

            var token = $"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXRhaWxzIjp7IkZpcnN0TmFtZSI6InN0cmluZyIsIkxhc3ROYW1lIjoic3RyaW5nIiwiRGF0ZU9mQmlydGgiOiJzdHJpbmcifSwiYmFua0RldGFpbHMiOnsiQWNjb3VudE5vIjoic3RyaW5nIiwiU29ydENvZGUiOiJzdHJpbmcifX0.-OrEBhhaGgkH0Zm8sdozx3VMqGYksu7zye-h6zqCFvI";
            var secretKey = $"XyNqk182E0veitxAOHBi";

            var result = encryptionService.GetDecodedToken<RequestBody>(decoder, token, secretKey);

            Assert.NotNull(result);
            Assert.IsType<RequestBody>(result);
            
        }

    }
}
