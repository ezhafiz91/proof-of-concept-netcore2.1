﻿
namespace Poc.Switch
{
    using System.Reflection;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Swashbuckle.AspNetCore.Swagger;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Internal;
    using Feature.JwtEncryption.Services.Implementations;
    using Feature.JwtEncryption.Services.Interfaces;
    using Feature.GoogleCalendar.Services.Implementations;
    using Feature.GoogleCalendar.Services.Interfaces;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            // Make sure appsettings.config is up to date
            Features = Configuration.GetSection("Dependencies:Features").Get<List<string>>();
            Foundations = Configuration.GetSection("Dependencies:Foundations").Get<List<string>>();
        }

        public IConfiguration Configuration { get; }

        public List<string> Features { get; }

        public List<string> Foundations { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Foundations (anything in foundation layer)
            if (Foundations.Any())
            {
                Foundations.ForEach(x => { services.AddMvc().AddApplicationPart(Assembly.Load(new AssemblyName(x))); });
            }

            // Features (any Features)
            if (Features.Any())
            {
                Features.ForEach(x => { services.AddMvc().AddApplicationPart(Assembly.Load(new AssemblyName(x))); });
            }

            // Services (any services)
            services.AddSingleton<IJWTService, JWTService>();
            services.AddSingleton<IGoogleCalendarApiService, GoogleCalendarApiService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Switch IT POC", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Switch IT POC"); });
            
            app.UseMvc();
        }
    }
}
