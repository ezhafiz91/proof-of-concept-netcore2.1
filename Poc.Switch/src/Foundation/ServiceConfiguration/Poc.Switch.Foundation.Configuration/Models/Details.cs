﻿namespace Poc.Switch.Foundation.Configuration.Models
{
    public class Details
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
    }
}
