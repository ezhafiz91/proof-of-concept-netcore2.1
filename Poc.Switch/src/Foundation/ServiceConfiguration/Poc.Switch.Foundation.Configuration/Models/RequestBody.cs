﻿
namespace Poc.Switch.Foundation.Configuration.Models
{
    public class RequestBody
    {
        public Details details { get; set; }
        public BankDetails bankDetails { get; set; }
    }
}
