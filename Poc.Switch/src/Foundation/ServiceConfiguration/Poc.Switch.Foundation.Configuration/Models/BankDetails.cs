﻿namespace Poc.Switch.Foundation.Configuration.Models
{
    public class BankDetails
    {
        public string AccountNo { get; set; }
        public string SortCode { get; set; }
    }
}
